import pandas as pd
from pydantic import BaseModel, Field, ValidationError
from datetime import time
from typing import List




class LogValidator(BaseModel):
    PID: int = Field(..., ge=0)
    USER: str = Field(..., max_length=32)
    TIME: time 


class ListableValidator(BaseModel):
    data_frame: List[LogValidator]



class Logger():
    """
    A class used to represent an Logger

    ...

    Attributes
    ----------
    DEFAULT_PATH : str
        The default path to file as *.json. 
    DEFAULT_LIMIT : int
        This value as default LIMIT for the SORTABLE method.
    DEFAULT_SORTING_BY_COLUMN : str
        The default COLUMN for sorting to the SORTABLE method.

    Methods
    -------
    sortable(self, COLUMN=DEFAULT_SORTING_BY_COLUMN, LIMIT = DEFAULT_LIMIT)
        This function returns the first n by LIMIT value via the sortable conditions as a COLUMN.
    """


    DEFAULT_PATH = "logs_example.json"
    DEFAULT_LIMIT = 10
    DEFAULT_SORTING_BY_COLUMN = "TIME"
    
    full_logs = None

    def __init__(self, path = DEFAULT_PATH):
        try:
            full_logs = pd.read_json(path)
            ListableValidator(data_frame=full_logs.to_dict("records")) # Pydantic *.json validator
            self.full_logs = full_logs
        except ValidationError as e:
            print(e)
        

    def __repr__(self) -> pd.DataFrame:
        return self.full_logs

    def __str__(self) -> str:
        return str(self.full_logs)

    def sortable(self, COLUMN=DEFAULT_SORTING_BY_COLUMN, LIMIT = DEFAULT_LIMIT) -> pd.DataFrame:
        return self.full_logs.sort_values(by=COLUMN, ascending=False).head(LIMIT)

#Logger(path="logs_example.json")
#print(logger.sortable(limit=1))





