import sys
from logger import Logger

  
if __name__ == '__main__':
    path = input("Enter the path to file as *.json: ") # for example: logs_example.json
    limit = int(input("Enter limit: ")) # for example: 3
    logger = Logger(path=path)
    print(logger.sortable(limit=limit))
   