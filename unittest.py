from logger import Logger 
import unittest   # The test framework
import pandas as pd


class Test_SortableLogger(unittest.TestCase):
    def test_sortable(self):
        logger = Logger()
        self.assertEqual(logger.sortable(LIMIT=1), pd.DataFrame({'PID': 1, 'USER': 'root', 'TIME': 12.30}))


if __name__ == '__main__':
    unittest.main()